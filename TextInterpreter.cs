﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace HeartHero.FE9
{
    public class TextInterpreter
    {
        // Path of Radiance uses SHIFT-JIS Encoding (Codepage 932)
        public static Encoding DefaultEncoding = Encoding.GetEncoding(932);

        private static string decodeText(byte[] binaryInput)
        {

            int entry_count;
            int table_index;

            byte[] header = new byte[32];
            byte[] body = new byte[binaryInput.Length - 32];

            for (int i = 0; i < 32; i++)
            {
                header[i] = binaryInput[i];
            }

            // Write body without header
            Array.Copy(binaryInput, 32, body, 0, binaryInput.Length - 32);

            byte[] entries = new byte[4];
            for (int i = 0; i < 4; i++)
            {
                entries[i] = header[12 + i];
            }

            // Write number of entries

            Array.Reverse(entries);
            entry_count = BitConverter.ToInt32(entries, 0);

            // Get start of pointer table

            byte[] index = new byte[4];
            for (int i = 0; i < 4; i++)
            {
                index[i] = header[4 + i];
            }

            Array.Reverse(index);
            table_index = BitConverter.ToInt32(index, 0);
            //Console.WriteLine(table_index);

            // 
            // String Converter
            //

            SortedList<int, string> master = new SortedList<int, string>();

            int name_i = table_index + (8 * entry_count);

            for (int cur_entry = 0; cur_entry < entry_count; cur_entry++)
            {

                index = new byte[4];
                for (int i = 0; i < 4; i++)
                {
                    index[i] = body[table_index + i + (cur_entry * 8)];
                }
                Array.Reverse(index);
                int string_index = BitConverter.ToInt32(index, 0);

                List<byte> str_raw = new List<byte>();
                int str_i = string_index;
                while (body[str_i] != 0)
                {
                    str_raw.Add(body[str_i]);
                    str_i++;
                }

                byte[] str_arr = str_raw.ToArray();
                string str_fin = DefaultEncoding.GetString(str_arr);

                // Internal names

                List<byte> name_raw = new List<byte>();
                while (body[name_i] != 0)
                {
                    name_raw.Add(body[name_i]);
                    name_i++;
                }
                name_i = name_i + 1;

                byte[] name_arr = name_raw.ToArray();
                string name_fin = DefaultEncoding.GetString(name_arr);

                // Commands

                str_fin = Regex.Replace(str_fin, @"\$w([0-9])", "[Pause=$1]", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\$c([0-9])([A-Z]+?[0-9]*?)\|", "[Face='$2' Pos='$1']", RegexOptions.Multiline);

                str_fin = Regex.Replace(str_fin, @"\$FCL_([A-Z]+?[0-9]*?)\|", "[LoadPortrait_L=$1]", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\$FC([A-Z]+?[0-9]*?)\|", "[LoadPortrait_S=$1]", RegexOptions.Multiline);

                str_fin = Regex.Replace(str_fin, @"\$F([A-Z])", "[FaceCommand=$1]", RegexOptions.Multiline | RegexOptions.IgnoreCase);
                str_fin = Regex.Replace(str_fin, @"\$F([0-9])", "[F_Target=$1]", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\$s([0-9])", "[S_Target=$1]", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\$d([0-9])", "[ClearTarget=$1]", RegexOptions.Multiline);

                str_fin = Regex.Replace(str_fin, @"\$B(.+?)\|", "[LoadBackground=$1]", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\$R(.+?)\|", "[Scene=$1]", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\$=([0-9]+)", "[Fade=$1]", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\$MC", "[FreezeMouth]", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\$MD", "[UnfreezeMouth]", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\$K", "[A]", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\$P", "[ClearBox]", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\$<", "[Trigger]", RegexOptions.Multiline);

                str_fin = name_fin + "\n\n" + str_fin;

                master.Add(string_index, str_fin + "[X]\n\n");

            }

            string[] master_arr = new string[entry_count];

            int sort_index = 0;
            foreach (KeyValuePair<int, string> kvp in master)
            {
                master_arr[sort_index] = kvp.Value;
                //Console.WriteLine(kvp.Key);
                sort_index++;

            }

            return string.Join(string.Empty, master_arr);
        }

        private static byte[] encodeText(string decodedInput)
        {
            
            Regex rgx = new Regex(@"(.+)\n\n([\s\S]*?)\[X\]\n\n", RegexOptions.Multiline | RegexOptions.IgnoreCase);

            int entry_count = rgx.Matches(decodedInput).Count;
            int name_size = 0;
            int current = 0;

            string[] name_array = new string[entry_count];
            int[] name_indexes = new int[entry_count];
            int[] table_indexes = new int[entry_count];
            List<byte> master = new List<byte>();

            foreach (Match match in rgx.Matches(decodedInput))
            {
                name_array[current] = match.Groups[1].Value;
                table_indexes[current] = master.Count;

                name_indexes[current] = name_size;
                name_size += name_array[current].Length + 1;

                string str_fin = match.Groups[2].Value;

                str_fin = Regex.Replace(str_fin, @"\[Pause=([0-9])\]", "$w$1", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[Face='([A-Z]+?[0-9]*?)' Pos='([0-9])'\]", "$c$2$1|", RegexOptions.Multiline);

                str_fin = Regex.Replace(str_fin, @"\[LoadPortrait_L=([A-Z]+?[0-9]*?)\]", "$FCL_$1|", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[LoadPortrait_S=([A-Z]+?[0-9]*?)\]", "$FC$1|", RegexOptions.Multiline);

                str_fin = Regex.Replace(str_fin, @"\[FaceCommand=([A-Z])\]", "$F$1", RegexOptions.Multiline | RegexOptions.IgnoreCase);
                str_fin = Regex.Replace(str_fin, @"\[F_Target=([0-9])\]", "$F$1", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[S_Target=([0-9])\]", "$s$1", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[ClearTarget=([0-9])\]", "$d$1", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[Scene=(.+?)\]", "$R$1|", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[LoadBackground=(.+?)\]", "$B$1|", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[Fade=([0-9]+?)\]", "$=$1", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[FreezeMouth\]", "$MC", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[UnfreezeMouth\]", "$MD", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[A\]", "$K", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[ClearBox\]", "$P", RegexOptions.Multiline);
                str_fin = Regex.Replace(str_fin, @"\[Trigger\]", "$<", RegexOptions.Multiline);

                master.AddRange(DefaultEncoding.GetBytes(str_fin));
                master.Add(0);

                while (master.Count % 4 != 0)
                {
                    master.Add(0);
                }

                current++;

            }

            // Writing Pointer Table

            int pointer_index = master.Count + 1;
            for (int i = 0; i < table_indexes.Length; i++)
            {
                byte[] point_index = BitConverter.GetBytes(table_indexes[i]);
                Array.Reverse(point_index);
                master.AddRange(point_index);

                byte[] name_index = BitConverter.GetBytes(name_indexes[i]);
                Array.Reverse(name_index);
                master.AddRange(name_index);
            }

            // Writing Name Keys

            for (int i = 0; i < name_array.Length; i++)
            {
                //Console.WriteLine(name_array[i]);
                master.AddRange(DefaultEncoding.GetBytes(name_array[i]));
                master.Add(0);
            }

            // Header Processing

            List<byte> header = new List<byte>();

            byte[] eof = BitConverter.GetBytes(master.Count + 32);
            Array.Reverse(eof);
            header.AddRange(eof);

            byte[] pointer_raw = BitConverter.GetBytes(pointer_index - 1);
            Array.Reverse(pointer_raw);
            header.AddRange(pointer_raw);

            header.AddRange(new byte[0x4]);

            byte[] entries_raw = BitConverter.GetBytes(entry_count);
            Array.Reverse(entries_raw);
            header.AddRange(entries_raw);

            header.AddRange(new byte[0x10]);

            master.InsertRange(0, header);

            // Filesize must be divisible by 16

            while (master.Count % 16 != 0)
            {
                master.Add(0);
            }

            return master.ToArray();
        }

        /// <summary>
        /// Reads and decodes a file from a byte array
        /// </summary>
        /// <param name="input">A byte array.</param>
        /// <returns>A decoded string of FE9 text data.</returns>
        public static string Decode(byte[] input)
        {
            return decodeText(input);
        }

        /// <summary>
        /// Reads and decodes a file from a string
        /// </summary>
        /// <param name="input">A string representing a valid path.</param>
        /// <returns>A decoded string of FE9 text data.</returns>
        public static string Decode(string input)
        {
            byte[] binaryInput = File.ReadAllBytes(input);
            return decodeText(binaryInput);
        }

        /// <summary>
        /// Reads and encodes the file from the provided path
        /// </summary>
        /// <param name="fileInput">A string representing a valid path.</param>
        /// <returns>A byte array containing FE9 text data.</returns>
        public static byte[] Encode(string fileInput)
        {
            string active_file = File.ReadAllText(fileInput, DefaultEncoding);
            return encodeText(active_file);
        }
    }
}
