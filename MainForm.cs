using System.Windows.Forms;
using System.IO;

namespace HeartHero.FE9
{

    public partial class MainForm : Form
    {

        public enum Mode
        {
            Encode,
            Decode
        }

        public MainForm()
        {
            InitializeComponent();
            groupBox1.AllowDrop = true;
            groupBox2.AllowDrop = true;
        }

        private void groupBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        private void decodeBox_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            processFiles(files, Mode.Decode);
        }

        private void encodeBox_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            processFiles(files, Mode.Encode);
        }

        private void processFiles(string[] files, Mode mode)
        {
            progress.Maximum = files.Length;
            Enabled = false;

            foreach (string file in files)
            {
                progress.Value += 1;
                switch (mode)
                {
                    case Mode.Decode:
                        File.WriteAllText(file.Replace(Path.GetExtension(file), ".txt"), TextInterpreter.Decode(file), TextInterpreter.DefaultEncoding);
                        break;
                    case Mode.Encode:
                        File.WriteAllBytes(file.Replace(Path.GetExtension(file), ".m"), TextInterpreter.Encode(file));
                        break;
                }
            }
            progress.Value = 0;
            Enabled = true;
            System.Media.SystemSounds.Beep.Play();
        }

    }
}